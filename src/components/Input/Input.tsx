import * as React from 'react';
import { InputHTMLAttributes } from 'react';
import { useFormContext, ValidationRules } from "react-hook-form";
import ErrorLabel from 'common/ErrorLabel';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  rules?: ValidationRules;
  defaultValue?: string | number;
}

export const Input: React.FC<InputProps> = ({ name = '', rules, defaultValue, ...props }) => {
  const context = useFormContext();
  const isError = () => context?.errors[name];
  const additonalProps = context ? { ref: rules && context ? context.register(rules) : context.register } : {}

  return (<>
    <input defaultValue={defaultValue} name={name} id={props.id ? props.id : name} {...props} {...additonalProps} />
    {isError() && <ErrorLabel name={name}/>}
  </>)
};
