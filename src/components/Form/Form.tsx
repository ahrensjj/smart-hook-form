import * as React from 'react';
import { FormProvider, SubmitHandler, useForm, UseFormMethods, Resolver } from 'react-hook-form';

export interface FormProps<T> {
  useForm?: UseFormMethods<T>;
  onSubmit?: SubmitHandler<T>;
  schema?:Resolver<T, any>;

}


export const Form: React.FC<FormProps<Record<string,any>>& React.DetailedHTMLProps<React.FormHTMLAttributes<HTMLFormElement>, HTMLFormElement> > = ({ onSubmit, onInvalid,schema, ...props }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const methods = props.useForm ? props.useForm : useForm({ mode: 'onChange', resolver:schema});
  const { handleSubmit } = methods;
  const additionProps = onSubmit ? {
    onSubmit: handleSubmit(onSubmit),
  } : {};


  return <FormProvider {...methods} >
    <form {...additionProps}>
      {props.children}
    </form>
  </FormProvider>
}
