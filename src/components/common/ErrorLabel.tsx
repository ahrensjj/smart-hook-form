import React from 'react'
import { useFormContext } from 'react-hook-form';

export interface ErrorLabelProps {
    name: string;
}

export const ErrorLabel: React.FC<ErrorLabelProps> = ({ name }) => {
    const context = useFormContext();
    const errors = context?.errors;

    return <label htmlFor={name}>{errors[name].message}</label>
}

export default ErrorLabel;