import * as React from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import ReactDatePicker, { ReactDatePickerProps } from 'react-datepicker'
import { DateValidation } from 'validation-types';
import DateValidationRules = DateValidation.DateValidationRules
import DefaultDate = DateValidation.DefaultDate

import "react-datepicker/dist/react-datepicker.css";
import ErrorLabel from 'common/ErrorLabel';

export interface DateInputProps extends Partial<ReactDatePickerProps> {
  rules?: DateValidationRules;
  defaultValue?: DefaultDate;
}

export const DateInput: React.FC<DateInputProps> = ({ name = '', rules, defaultValue, ...props }) => {
  const context = useFormContext();
  const isError = () => context?.errors[name];

  return (
    <>
      {
        context ?
          <Controller
            control={context.control}
            name={name}
            defaultValue={defaultValue}
            rules={rules}
            render={({ onChange, onBlur, value, name }) => (
              <ReactDatePicker
                name={name}
                onChange={onChange}
                onBlur={onBlur}
                selected={value}
                {...props}
              />
            )}
          /> : <ReactDatePicker name={name} id={props.id ? props.id : name} onChange={() => console.warn(`no onchange implemented for ${name}`)} {...props} />
      }
      {isError() && <ErrorLabel name={name} />}
    </>)
};
