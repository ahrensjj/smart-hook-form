import React from 'react'
import * as rhf from 'react-hook-form'
import { ReactDatePicker } from 'react-datepicker'
import { yupResolver } from '@hookform/resolvers';
import { number, object, string, date, InferType } from 'yup'

import { Input, DateInput, Form, Select } from 'smart-hook-form'
const tomorrow = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));


const onSubmit = data => console.log('Submit:', data)
const dateSchema = object({
  name: string().required('required'),
  email: string().email().notRequired(),
  date: date().required().min(new Date(), 'must be in the future'),
  select: string().required().notOneOf(['B'], 'B is not allowed')
})

const App = () => {

  return (
      <Form onSubmit={onSubmit} schema={yupResolver(dateSchema)}>
        <Input type="text" name="name" />
        <Input type="email" name="email" />
        <DateInput name="date" defaultValue={tomorrow} />
        <Select name="select" defaultValue="C">
          <option>A</option>
          <option>B</option>
          <option>C</option>
        </Select>
        <button type={'submit'}>submit</button>
      </Form>
  )
}
export default App
