# smart-hook-form

> bundle of smart form components

[![NPM](https://img.shields.io/npm/v/smart-hook-form.svg)](https://www.npmjs.com/package/smart-hook-form) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save smart-hook-form
```

## Usage

```
import * as React from 'react'

import { useMyHook } from 'smart-hook-form'

const Example = () => {
  const example = useMyHook()
  return (
    <div>
      {example}
    </div>
  )
}
```

## License

MIT © [ahrensjj](https://github.com/ahrensjj)

---

This hook is created using [create-react-hook](https://github.com/hermanya/create-react-hook).

npm i -S smart-hook-form@file:../smart-hook-form

In app:
cd node_modules/react && npm link
cd node_modules/react-dom && npm link

in Lib:
npm link react
npm link react-dom